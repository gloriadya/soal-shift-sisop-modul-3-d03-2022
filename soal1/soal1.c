#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <pthread.h> 

const char b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

size_t b64_decoded_size(const char *in)
{
	size_t len;
	size_t ret;
	size_t i;

	if (in == NULL)
		return 0;

	len = strlen(in);
	ret = len / 4 * 3;

	for (i=len; i-->0; ) {
		if (in[i] == '=') {
			ret--;
		} else {
			break;
		}
	}

	return ret;
}

void b64_generate_decode_table()
{
	int    inv[80];
	size_t i;

	memset(inv, -1, sizeof(inv));
	for (i=0; i<sizeof(b64chars)-1; i++) {
		inv[b64chars[i]-43] = i;
	}
}

int b64_isvalidchar(char c)
{
	if (c >= '0' && c <= '9')
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c >= 'a' && c <= 'z')
		return 1;
	if (c == '+' || c == '/' || c == '=')
		return 1;
	return 0;
}

int b64_decode(const char *in, unsigned char *out, size_t outlen)
{
	size_t len;
	size_t i;
	size_t j;
	int    v;

	if (in == NULL || out == NULL)
		return 0;

	len = strlen(in);
	if (outlen < b64_decoded_size(in) || len % 4 != 0)
		return 0;

	for (i=0; i<len; i++) {
		if (!b64_isvalidchar(in[i])) {
			return 0;
		}
	}

	for (i=0, j=0; i<len; i+=4, j+=3) {
		v = b64invs[in[i]-43];
		v = (v << 6) | b64invs[in[i+1]-43];
		v = in[i+2]=='=' ? v << 6 : (v << 6) | b64invs[in[i+2]-43];
		v = in[i+3]=='=' ? v << 6 : (v << 6) | b64invs[in[i+3]-43];

		out[j] = (v >> 16) & 0xFF;
		if (in[i+2] != '=')
			out[j+1] = (v >> 8) & 0xFF;
		if (in[i+3] != '=')
			out[j+2] = v & 0xFF;
	}

	return 1;
}

void zip_download(){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); 
    }

    if (child_id == 0) {
        
        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-O", "quote.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    } else {
        
        while ((wait(&status)) > 0);

        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-O", "music.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
}


void create_dir(char* dir){ 
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); 

    if(child_id == 0){
        char *argv[] = {"mkdir", "-p", dir, NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    }
    else while ((wait(&status)) > 0);
    
}

void unzip_file(char* dir, char* pass){ 
    pid_t child_id;
    int status;

    char zip[20];
    sprintf(zip, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); /
    }

    if(child_id == 0){
        if(pass == NULL){
            char* argv[] = {"unzip", "-q","-n", zip, "-d", dir, NULL};
            execv("/bin/unzip",argv);
            exit(0);
        }
        else{
            char* argv[] = {"unzip", "-P", pass, zip, NULL};
            execv("/bin/unzip",argv);
            exit(0);
        }

    }
    else while ((wait(&status)) > 0);
    
}

void zip_file(char *dir){ 
    pid_t child_id;
    int status;

    char zip[20];
    sprintf(zip, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); 
    }

    if(child_id == 0){
        char* argv[] = {"zip", "-P","mihinomenestnaufal", "-r", zip, dir, NULL};
        execv("/bin/zip",argv);
        exit(0);
    }
    else while ((wait(&status)) > 0);
}

void file_create(char* filename){
    FILE *fptr;
    fptr = fopen(filename, "w");
    if(strcmp(filename, "hasil/no.txt") == 0)fprintf(fptr, "no");
    fclose(fptr);
}

void file_move(char* dest, char* src){
    FILE *fptr1, *fptr2;
    char temp;
    fptr1 = fopen(src, "r");
    fptr2 = fopen(dest, "w");
    while(fscanf(fptr1, "%c", &temp) != EOF) fprintf(fptr2, "%c", temp);
    fclose(fptr1);
    fclose(fptr2);
    remove(src);
}

void *decode(void *file){
    char *filename, buffer[1024], *decoded, loc[20];
    filename = (char *) file;
    size_t len;
    FILE *fptr;
    fptr = fopen(filename, "r");
    fscanf(fptr, "%s", buffer);
    fclose(fptr);

    len = b64_decoded_size(buffer) + 1;
    decoded = (char*) malloc(len*sizeof(char));

    if(b64_decode(buffer, (unsigned char *) decoded, len)){
        decoded[len] = '\0';
        if(filename[0] == 'm') strcpy(loc, "music.txt");
        else if(filename[0] == 'q') strcpy(loc, "quote.txt");
        fptr = fopen(loc, "a");
        fprintf(fptr, "%s\n", decoded);
        fclose(fptr);
    }

    
}

int main(){
    pthread_t thread[9];
    int  iret[9];
    char file_loc[50];

    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); 
    }

    if (child_id == 0) {
        zip_download();
    }
    else{
        while ((wait(&status)) > 0);
        create_dir("quote");
        create_dir("music");
        unzip_file("quote", NULL);
        unzip_file("music", NULL);
        file_create("quote.txt");
        file_create("music.txt");
    }

    for(int i=1; i<10; i++){
        sprintf(file_loc, "music/m%d.txt", i);
        iret[i-1] = pthread_create(&thread[i-1], NULL, decode, (void*) file_loc);
        if(iret[i-1]){
            fprintf(stderr,"Error - pthread_create() return code: %d\n",iret[i-1]);
            exit(EXIT_FAILURE);
        }
        pthread_join( thread[i-1], NULL);
    }

    for(int i=1; i<10; i++){
        sprintf(file_loc, "quote/q%d.txt", i);
        iret[i-1] = pthread_create(&thread[i-1], NULL, decode, (void*) file_loc);
        if(iret[i-1]){
            fprintf(stderr,"Error - pthread_create() return code: %d\n",iret[i-1]);
            exit(EXIT_FAILURE);
        }
        pthread_join( thread[i-1], NULL);
    }

    create_dir("hasil");
    file_move("hasil/music.txt", "music.txt");
    file_move("hasil/quote.txt", "quote.txt");
    zip_file("hasil");
    unzip_file("hasil.zip", "mihinomenestnaufal");
    file_create("hasil/no.txt");
    zip_file("hasil");
    

    exit(EXIT_SUCCESS);
}
