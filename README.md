# soal-shift-sisop-modul-3-D03-2022

Kelompok D 03 \
Anggota Kelompok 
|Nama                   |     NRP|
|-----------------------|----------------------|
|Naufal Fabian Wibowo    |    05111940000223|
|Gloria Dyah Pramesti    |    5025201033|
|Selfira Ayu Sheehan     |    5025201174|

# Laporan

Soal 1  
Penjelasan  
* 1a. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan
nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini
dilakukan dengan bersamaan menggunakan thread.
```
void zip_download(){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child
        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-O", "quote.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    } else {
        // this is parent
        while ((wait(&status)) > 0);

        char *argv[] = {"wget", "-q", "https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-O", "music.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
}


void create_dir(char* dir){ // bikin directory baru
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char *argv[] = {"mkdir", "-p", dir, NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    }
    else while ((wait(&status)) > 0);
    
}

void unzip_file(char* dir, char* pass){ // unzip file
    pid_t child_id;
    int status;

    char zip[20];
    sprintf(zip, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        if(pass == NULL){
            char* argv[] = {"unzip", "-q","-n", zip, "-d", dir, NULL};
            execv("/bin/unzip",argv);
            exit(0);
        }
        else{
            char* argv[] = {"unzip", "-P", pass, zip, NULL};
            execv("/bin/unzip",argv);
            exit(0);
        }

    }
    else while ((wait(&status)) > 0);
    
}

void zip_file(char *dir){ // 1d
    pid_t child_id;
    int status;

    char zip[20];
    sprintf(zip, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char* argv[] = {"zip", "-P","mihinomenestnaufal", "-r", zip, dir, NULL};
        execv("/bin/zip",argv);
        exit(0);
    }
    else while ((wait(&status)) > 0);
}

```

* 1b. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu
file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada
saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan
music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
 ```
const char b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

size_t b64_decoded_size(const char *in)
{
	size_t len;
	size_t ret;
	size_t i;

	if (in == NULL)
		return 0;

	len = strlen(in);
	ret = len / 4 * 3;

	for (i=len; i-->0; ) {
		if (in[i] == '=') {
			ret--;
		} else {
			break;
		}
	}

	return ret;
}

void b64_generate_decode_table()
{
	int    inv[80];
	size_t i;

	memset(inv, -1, sizeof(inv));
	for (i=0; i<sizeof(b64chars)-1; i++) {
		inv[b64chars[i]-43] = i;
	}
}

int b64_isvalidchar(char c)
{
	if (c >= '0' && c <= '9')
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c >= 'a' && c <= 'z')
		return 1;
	if (c == '+' || c == '/' || c == '=')
		return 1;
	return 0;
}

int b64_decode(const char *in, unsigned char *out, size_t outlen)
{
	size_t len;
	size_t i;
	size_t j;
	int    v;

	if (in == NULL || out == NULL)
		return 0;

	len = strlen(in);
	if (outlen < b64_decoded_size(in) || len % 4 != 0)
		return 0;

	for (i=0; i<len; i++) {
		if (!b64_isvalidchar(in[i])) {
			return 0;
		}
	}

	for (i=0, j=0; i<len; i+=4, j+=3) {
		v = b64invs[in[i]-43];
		v = (v << 6) | b64invs[in[i+1]-43];
		v = in[i+2]=='=' ? v << 6 : (v << 6) | b64invs[in[i+2]-43];
		v = in[i+3]=='=' ? v << 6 : (v << 6) | b64invs[in[i+3]-43];

		out[j] = (v >> 16) & 0xFF;
		if (in[i+2] != '=')
			out[j+1] = (v >> 8) & 0xFF;
		if (in[i+3] != '=')
			out[j+2] = v & 0xFF;
	}

	return 1;
}

 ```

 ```

 ```


* 1c. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama
hasil.
```
void file_move(char* dest, char* src){
    FILE *fptr1, *fptr2;
    char temp;
    fptr1 = fopen(src, "r");
    fptr2 = fopen(dest, "w");
    while(fscanf(fptr1, "%c", &temp) != EOF) fprintf(fptr2, "%c", temp);
    fclose(fptr1);
    fclose(fptr2);
    remove(src);
}

void *decode(void *file){
    char *filename, buffer[1024], *decoded, loc[20];
    filename = (char *) file;
    size_t len;
    FILE *fptr;
    fptr = fopen(filename, "r");
    fscanf(fptr, "%s", buffer);
    fclose(fptr);

    len = b64_decoded_size(buffer) + 1;
    decoded = (char*) malloc(len*sizeof(char));

    if(b64_decode(buffer, (unsigned char *) decoded, len)){
        decoded[len] = '\0';
        if(filename[0] == 'm') strcpy(loc, "music.txt");
        else if(filename[0] == 'q') strcpy(loc, "quote.txt");
        fptr = fopen(loc, "a");
        fprintf(fptr, "%s\n", decoded);
        fclose(fptr);
    }
 ```

* 1d. Folder hasil di-zip menjadi file hasil.zip dengan password &#39;mihinomenest[Nama
user]&#39;. (contoh password : mihinomenestnovak)
```
void zip_file(char *dir){ // 1d
    pid_t child_id;
    int status;

    char zip[20];
    sprintf(zip, "%s.zip", dir);

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(child_id == 0){
        char* argv[] = {"zip", "-P","mihinomenestnaufal", "-r", zip, dir, NULL};
        execv("/bin/zip",argv);
        exit(0);
    }
    else while ((wait(&status)) > 0);
}

 ```
* 1e. Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt
dengan tulisan &#39;No&#39; pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt
menjadi hasil.zip, dengan password yang sama seperti sebelumnya.
```
void file_create(char* filename){
    FILE *fptr;
    fptr = fopen(filename, "w");
    if(strcmp(filename, "hasil/no.txt") == 0)fprintf(fptr, "no");
    fclose(fptr);
 ```
  ```
    unzip_file("hasil.zip", "mihinomenestnaufal");
    file_create("hasil/no.txt");
    zip_file("hasil");
 ```

* 1. Hasil output
 ![Screenshot_2022-04-17_215320](/uploads/e5c506474f092f0fbcfaaa2da16dbb77/Screenshot_2022-04-17_215320.png)

![Screenshot_2022-04-17_215301](/uploads/7496f66a3dcc1eb09c27cb3bf496cb70/Screenshot_2022-04-17_215301.png)

![Screenshot_2022-04-17_215239](/uploads/59af47f7126ec4eb1a0a5816620acec9/Screenshot_2022-04-17_215239.png)

![Screenshot_2022-04-17_215205](/uploads/a2e5b025ce6f9a1a1d64b014ebb3f48d/Screenshot_2022-04-17_215205.png)


Soal 2  
Penjelasan  

Soal 3  
Penjelasan  
* 3a. Mengekstrak file dengan library zip.h dan unzip.h. File yang sudah diekstrakpun diurutkan.
```
char *path = "hartakarun.zip";
    if (argc < 2) {
        printf("usage:\n%s hartakarun.zip\n", argv[0]);
        return -1;
    }

    unzFile *zipfile = unzOpen(path);
    if (zipfile == NULL) {
        printf("%s: not found\n");
        return -1;
    }

    unz_global_info global_info;
    if (unzGetGlobalInfo(zipfile, &global_info) != UNZ_OK) {
        printf("could not read file global info\n");
        unzClose(zipfile);
        return -1;
    }

    char read_buffer[READ_SIZE];

    uLong i;
    for (i = 0; i < global_info.number_entry; ++i) {
        unz_file_info file_info;
        char filename[MAX_FILENAME] = "../hartakarun.zip";
        if (unzGetCurrentFileInfo(
            zipfile,
            &file_info,
            filename,
            MAX_FILENAME,
            NULL, 0, NULL, 0) != UNZ_OK) {
            printf("could not read file info\n");
            unzClose(zipfile);
            return -1;
        }

        const size_t filename_length = strlen( filename );
        if (filename[filename_length-1] == dir_delimter) {
            printf("dir:%s\n", filename);
        } else {
            printf("file:%s\n", filename);
            if (unzOpenCurrentFile(zipfile) != UNZ_OK){
                printf("could not open file\n");
                unzClose(zipfile);
                return -1;
            }

            FILE *out = fopen(filename, "wb");
            if (out == NULL){
                printf("could not open destination file\n");
                unzCloseCurrentFile(zipfile);
                unzClose(zipfile);
                return -1;
            }

            int error = UNZ_OK;
            do {
                error = unzReadCurrentFile(zipfile, read_buffer, READ_SIZE);
                if (error < 0) {
                    printf("error %d\n", error);
                    unzCloseCurrentFile(zipfile);
                    unzClose(zipfile);
                    return -1;
                }

                if (error > 0) {
                    fwrite(read_buffer, error, 1, out);
                }
            } while (error > 0);

            fclose(out);
        }

        unzCloseCurrentFile(zipfile);

        if ((i+1) < global_info.number_entry) {
            if (unzGoToNextFile(zipfile) != UNZ_OK) {
                printf("cound not read next file\n");
                unzClose(zipfile);
                return -1;
            }
        }
    }
    unzClose(zipfile);
```  
* 3b. Melakukan pengecekan jenis file - demgam menggunakan pencarian untuk memasukkan file sesuai pada folder ekstensinya. Jika file tidak memiliki ekstensi maka langsung dimasukkan ke unknown

```
char dir[PATH_MAX];
    strcat(dir, "/hartakarun");

    char *filename = strrchr(directory, '/'), directoryCurrent[1000], directoryNew[1000], file[1000], directoryDefault[1000];
    
    if (strrchr(directory, '/')[1] == '.') {
        strcpy(directoryCurrent, "Hidden");
    } else if (strstr(directory, ".") != NULL) {
        strcpy(file, directory);
        strtok(file, ".");
        char *token = strtok(NULL, "");

        for (int i = 0; token[i]; i++) {
            token[i] = tolower(token[i]);
        }
        strcpy(directoryCurrent, token);
    } else {
        strcpy(directoryCurrent, "Unknown");
    }
```  

* 3c. Untuk mempercepat, nama diubah lalu dilakukan pengecekan.

```
snprintf(path, 1000, "%s/%s", directory, readFile->d_name); 

            if (stat(path, &buffer) == 0) {
                pthread_t thread;
                pthread_create(&thread, NULL, changeFile, (void *)path);
                pthread_join(thread, 0);
            }

            if (S_ISREG(buffer.st_mode)) {
                pthread_t thread;
                pthread_create(&thread, NULL, changeFile, (void *)path);
                pthread_join(thread, NULL);
            }
```  

* 3d. Folder di-zip menggunakan execv, lalu membuat connection pada file zipnya.

```
if (child_id == 0) {
        char *argv[]={"zip","-mr","hartakarun.zip","/soal3/hartakarun", NULL};
        execv("/usr/bin/zip",argv);
    } else {
        while ((wait(&status)) > 0);
        createConnection("hartakarun.zip");
    }
```  

* 3e. Melakukan pengecekkan port, membuat socket, mengecek connection, lalu mengirimkan file dengan fungsi send.

```
int sending(int socket, char *filename) {
    char fpath[1024], buffer[1024] = {0};
    int sizeFile;

    strcpy(fpath, "./");
    strcat(fpath, filename);
    FILE *file = fopen(fpath, "r");
    
    if (file != NULL) {
        bufferOnset(buffer, 1024);
    } else {
        printf("File not found: %s\n", filename);
        return -1;
    }

    sizeFile = fread(buffer, sizeof(char)

    while ((sizeFile, 1024, file)) > 0) {
        if (send(socket, buffer, sizeFile, 0) < 0) {
            fprintf(stderr, "Failed to send file: %s.\n", filename);
            break;
        }
        (buffer, 1024);
    }
    fclose(file);
    return 0;
}

socketCheck = socket(AF_INET, SOCK_STREAM, 0)
    if (socketCheck < 0) {
        printf("\n Error Creating Socket \n");
        return -1;
    }

    memset(&serverAddress, '0', sizeof(serverAddress));

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &serverAddress.sin_addr)<=0) {
        printf("\nInvalid address - Address not supported \n");
        return -1;
    }

    if (connect(socketCheck, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0) {
        printf("\nFailed to Connect!\n");
        return -1;
    }

    send(socketCheck , hello , strlen(hello) , 0 );
    sending(sock, filename);
```  

Hasil:


![1](/uploads/d054a50b8384aa217f08f8913c307060/1.jpg)

![2](/uploads/978c59dbac91503146fba1fa47573f0b/2.jpg)

![3](/uploads/01868a3acf715af26ec903e26daf6352/3.jpg)

![4](/uploads/ba71730ae94d8ba2c0a8f70d068084e6/4.jpg)

![5](/uploads/aca45a2edc1fe00b4c48e206ea031132/5.jpg)

![6](/uploads/e6cdac4b8fb0967fa625a9c5d5827d14/6.jpg)
